# Notes

## Links

- Convert SQL from mysql to postgresql http://www.sqlines.com/online


## Applications

- Navicat Premium

## Codes

```php
'Datasources' => [
        'default' => [
            'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Postgres',
            'persistent' => false,
            'host' => 'localhost',
            //'port' => 'non_standard_port_number',
             'username' => 'tedbundyjr',
            'password' => '',
            'database' => 'cakephp-blog-postgresql',
            //'encoding' => 'utf8mb4',
             'timezone' => 'UTC',
            'flags' => [],
            'cacheMetadata' => true,
            'log' => false,
            'quoteIdentifiers' => false,
            //'init' => ['SET GLOBAL innodb_stats_on_metadata = 0'],
            'url' => env('DATABASE_URL', null),
        ],
  ],
```

