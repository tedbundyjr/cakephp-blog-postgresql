<?php
use Migrations\AbstractSeed;

/**
 * Article seed.
 */
class ArticleSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'title' => 'The title',
                'body' => 'This is the article body.',
                'created' => '2019-03-18 11:56:00',
                'modified' => NULL,
            ],
            [
                'id' => 2,
                'title' => 'A title once again',
                'body' => 'And the article body follows.',
                'created' => '2019-03-18 11:56:00',
                'modified' => NULL,
            ],
            [
                'id' => 3,
                'title' => 'Title strikes back',
                'body' => 'This is really exciting! Not.',
                'created' => '2019-03-18 11:56:00',
                'modified' => NULL,
            ],
        ];

        $table = $this->table('articles');
        $table->insert($data)->save();
    }
}
