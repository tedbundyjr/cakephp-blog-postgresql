/* First, create our articles table: */
CREATE SEQUENCE articles_seq;

CREATE TABLE articles (
    id INT CHECK (id > 0) DEFAULT NEXTVAL ('articles_seq') PRIMARY KEY,
    title VARCHAR(50),
    body TEXT,
    created TIMESTAMP(0) DEFAULT NULL,
    modified TIMESTAMP(0) DEFAULT NULL
);

/* Then insert some articles for testing: */
INSERT INTO articles (title,body,created)
    VALUES ('The title', 'This is the article body.', NOW());
INSERT INTO articles (title,body,created)
    VALUES ('A title once again', 'And the article body follows.', NOW());
INSERT INTO articles (title,body,created)
    VALUES ('Title strikes back', 'This is really exciting! Not.', NOW());


